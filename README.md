# OpenML dataset: Foreign-Exchange-Rate-(1994---2020)

https://www.openml.org/d/43821

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The currency exchange rate is a key determinants of a country's relative level of economic health. It plays an essential role in a country's level of trade, which is critical to most every free market economy in the world. 
Content
This dataset starts from 3 Jan 1994 to 28 Feb 2020. This is a raw dataset, with the presence of blank values.  
Acknowledgements
This dataset is obtained from International Monetary Fund (IMF) from this website. This dataset won't be possible without this website. Kudos to those that help to obtain and manage the dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43821) of an [OpenML dataset](https://www.openml.org/d/43821). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43821/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43821/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43821/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

